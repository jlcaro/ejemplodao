package com.ejemplo.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.ejemplo.dto.Lenguaje;
import com.ejemplo.dao.DAO;

public class LenguajeDAO extends DAO<Lenguaje> {

    public Lenguaje create(Lenguaje obj) {
		try {
			ResultSet result = this	.connect.createStatement(
                  		ResultSet.TYPE_SCROLL_INSENSITIVE, 
                  		ResultSet.CONCUR_UPDATABLE
                  ).executeQuery(
                      "SELECT NEXTVAL('Lenguaje_len_id_seq') as id"
                  );
                  
			if(result.first()){
				long id = result.getLong("id");
    			PreparedStatement prepare = this	.connect
                      .prepareStatement(
                      	"INSERT INTO Lenguaje (len_id, len_nom) VALUES(?, ?)"
                      );
				prepare.setLong(1, id);
				prepare.setString(2, obj.getNombre());
				
				prepare.executeUpdate();
				obj = this.find(id);	
				
			}
	    } catch (SQLException e) {
	            e.printStackTrace();
	    }
	    return obj;
	}
	
	public Lenguaje find(long id) {
		Lenguaje leng = new Lenguaje();
		try {
            ResultSet result = this.connect.createStatement(
                            ResultSet.TYPE_SCROLL_INSENSITIVE, 
                              ResultSet.CONCUR_UPDATABLE
                           ).executeQuery(
                              "SELECT * FROM Lenguaje WHERE len_id = " + id
                           );
            if(result.first())
            		leng = new Lenguaje(id, result.getString("len_nom"));
		    } catch (SQLException e) {
		            e.printStackTrace();
		    }
		   return leng;
	}
	
	public Lenguaje update(Lenguaje obj) {
		try {
            this.connect.createStatement(
            	ResultSet.TYPE_SCROLL_INSENSITIVE, 
                ResultSet.CONCUR_UPDATABLE
             ).executeUpdate(
            	"UPDATE Lenguaje SET len_nom = '" + obj.getNombre() + "'"+
            	" WHERE len_id = " + obj.getId()
             );	
			obj = this.find(obj.getId());
	    } catch (SQLException e) {
	            e.printStackTrace();
	    }	    
	    return obj;
	}

	public void delete(Lenguaje obj) {
		try {
            this.connect.createStatement(
                     ResultSet.TYPE_SCROLL_INSENSITIVE, 
                     ResultSet.CONCUR_UPDATABLE
                ).executeUpdate(
                     "DELETE FROM Lenguaje WHERE len_id = " + obj.getId()
                );		
	    } catch (SQLException e) {
            e.printStackTrace();
	    }
	}
}
