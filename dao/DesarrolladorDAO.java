package com.ejemplo.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.ejemplo.dto.Desarrollador;
import com.ejemplo.dto.Lenguaje;
import com.ejemplo.dao.AbstractDAOFactory;
import com.ejemplo.dao.DAO;
import com.ejemplo.dao.FactoryType;

public class DesarrolladorDAO extends DAO<Desarrollador> {

    public Desarrollador create(Desarrollador obj) {
		
		try {
			 		
			if(obj.getLenguaje().getId() == 0){
				DAO<Lenguaje> lenguajeDAO = AbstractDAOFactory.getFactory(FactoryType.DAO_FACTORY).getLenguajeDAO();
				obj.setLenguaje(lenguajeDAO.create(obj.getLenguaje()));
			}
            ResultSet result = this	.connect.createStatement(
            		ResultSet.TYPE_SCROLL_INSENSITIVE, 
            		ResultSet.CONCUR_UPDATABLE
                ).executeQuery(
        			"SELECT NEXTVAL('desarrollador_dev_id_seq') as id"
                );
			if(result.first()){
				long id = result.getLong("id");
                PreparedStatement prepare = this.connect.prepareStatement(
            			"INSERT INTO desarrollador (dev_id, dev_nom, dev_ape, dev_len_id)"+
            			"VALUES(?, ?, ?, ?)"
                    );
				prepare.setLong(1, id);
				prepare.setString(2, obj.getNombre());
				prepare.setString(3, obj.getApellido());
				prepare.setLong(4, obj.getLenguaje().getId());
				
				prepare.executeUpdate();
				obj = this.find(id);	
				
			}
	    } catch (SQLException e) {
	            e.printStackTrace();
	    }
	    return obj;
	}
	
	public Desarrollador find(long id) {
		
		Desarrollador dev = new Desarrollador();
		try {
            ResultSet result = this .connect.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE, 
					ResultSet.CONCUR_READ_ONLY
				).executeQuery(
					"SELECT * FROM desarrollador WHERE dev_id = " + id
				);
            if(result.first())
                dev = new Desarrollador(
                	id, 
                	result.getString("dev_nom"), 
                	result.getString("dev_ape"),
                	new LenguajeDAO().find(result.getLong("dev_len_id"))
                );
		    } catch (SQLException e) {
	            e.printStackTrace();
		    }
		   return dev;

	}

	public Desarrollador update(Desarrollador obj) {
		
		try{	
			DAO<Lenguaje> lenguajeDAO = AbstractDAOFactory.getFactory(FactoryType.DAO_FACTORY).getLenguajeDAO();			
			if(obj.getLenguaje().getId() == 0){				
				obj.setLenguaje(lenguajeDAO.create(obj.getLenguaje()));
			}
			lenguajeDAO.update(obj.getLenguaje());
			
			this.connect	
	            .createStatement(
                	ResultSet.TYPE_SCROLL_INSENSITIVE, 
                    ResultSet.CONCUR_UPDATABLE
                 ).executeUpdate(
                	"UPDATE desarrollador SET dev_nom = '" + obj.getNombre() + "',"+
                	" dev_ape = '" + obj.getApellido() + "',"+
                	" dev_len_id = '" + obj.getLenguaje().getId() + "'"+
                	" WHERE dev_id = " + obj.getId()
                 );

			obj = this.find(obj.getId());
	    } catch (SQLException e) {
	            e.printStackTrace();
	    }
		return obj;
	}
	

	public void delete(Desarrollador obj) {
		try {
			this.connect.createStatement(
				ResultSet.TYPE_SCROLL_INSENSITIVE, 
				ResultSet.CONCUR_UPDATABLE
			).executeUpdate(
				"DELETE FROM desarrollador WHERE dev_id = " + obj.getId()
			);
	    } catch (SQLException e) {
	            e.printStackTrace();
	    }
	}
	
}