package com.ejemplo.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.ejemplo.dto.Desarrollador;
import com.ejemplo.dto.Lenguaje;
import com.ejemplo.dto.Empresa;
import com.ejemplo.dao.AbstractDAOFactory;
import com.ejemplo.dao.DAO;
import com.ejemplo.dao.FactoryType;

public class EmpresaDAO extends DAO<Empresa> {

	public Empresa create(Empresa obj) {
		try{
			
			ResultSet result = this	.connect.createStatement(
                  		ResultSet.TYPE_SCROLL_INSENSITIVE, 
                  		ResultSet.CONCUR_UPDATABLE
                  ).executeQuery(
                  		"SELECT NEXTVAL('empresa_emp_id_seq') as id"
                  );
			if(result.first()){
				long id = result.getLong("id");
     			PreparedStatement prepare = this.connect.prepareStatement(
            			"INSERT INTO empresa (emp_id, emp_nombre)"+
            			"VALUES(?, ?)"
            		);
				
				prepare.setLong(1, id);
				prepare.setString(2, obj.getNombre());
				prepare.executeUpdate();
				
				for(Desarrollador dev : obj.getListDesarrollador()){
					if(dev.getId() == 0){
						DAO<Desarrollador> desarrolladorDAO = AbstractDAOFactory.getFactory(FactoryType.DAO_FACTORY).getDesarrolladorDAO();
						dev = desarrolladorDAO.create(dev);
					}
					
					ResultSet result2 = this.connect.createStatement(
                            ResultSet.TYPE_SCROLL_INSENSITIVE, 
                            ResultSet.CONCUR_UPDATABLE
                        ).executeQuery(
                            "SELECT NEXTVAL('j_emp_dev_jsd_id_seq') as id"
                        );
					if(result2.first()){
						long id2 = result2.getLong("id");
      						PreparedStatement prepare2 = this .connect.prepareStatement(		
	      						"INSERT INTO j_emp_dev (jsd_id, jsd_emp_k, jsd_dev_k)"+
	      						" VALUES(?, ?, ?)"
                            );
						prepare2.setLong(1, id2);
						prepare2.setLong(2, id);
						prepare2.setLong(3, dev.getId());
						prepare2.executeUpdate();
					}
				}
				
				obj = this.find(id);	
				
			}
	    } catch (SQLException e) {
	            e.printStackTrace();
	    }
	    return obj;
		
	}
	
	public Empresa find(long id) {
		Empresa empresa = new Empresa();                
        
        try {
                ResultSet result = this.connect.createStatement(
                                             ResultSet.TYPE_SCROLL_INSENSITIVE, 
                                             ResultSet.CONCUR_UPDATABLE
                                        ).executeQuery(
                                            "select * from empresa "+
                                            " left join j_emp_dev on jsd_emp_k = emp_id AND emp_id = "+ id +
                                            " inner join desarrollador on jsd_dev_k = dev_id"
                                        );

                if(result.first()){
                	DesarrolladorDAO devDao = new DesarrolladorDAO();
                    ArrayList<Desarrollador> listDesarrollador = new ArrayList<Desarrollador>();
                    
                    result.beforeFirst();
                    while(result.next() && result.getLong("jsd_dev_k") != 0)
                    	listDesarrollador.add(devDao.find(result.getLong("jsd_dev_k")));
                    
                    result.first();
                    empresa = new Empresa(id, result.getString("emp_nombre"), listDesarrollador);
                }
        } catch (SQLException e) {
                e.printStackTrace();
        }
        return empresa;

	}
	public Empresa update(Empresa obj) {
		
		try{
			
			PreparedStatement prepare = this .connect
											 .prepareStatement(
                                            	"UPDATE empresa SET emp_nombre = '"+ obj.getNombre() +"'"+
                                            	" WHERE emp_id = " + obj.getId()
                                            );
			
			prepare.executeUpdate();
			
			for(Desarrollador dev : obj.getListDesarrollador()){
				
				DAO<Desarrollador> desarrolladorDAO = AbstractDAOFactory    .getFactory(FactoryType.DAO_FACTORY)
                                                                                    .getDesarrolladorDAO();

				
				//Si l'objet n'existe pas, on le créé avec sa jointure
				if(dev.getId() == 0){
					
					dev = desarrolladorDAO.create(dev);

					//On récupère la prochaine valeur de la séquence
					ResultSet result2 = this   .connect
                                               .createStatement(
                                            		ResultSet.TYPE_SCROLL_INSENSITIVE, 
                                            		ResultSet.CONCUR_UPDATABLE
                                                ).executeQuery(
                                            		"SELECT NEXTVAL('j_emp_dev_jsd_id_seq') as id"
                                                );
					if(result2.first()){
					
						long id2 = result2.getLong("id");
						PreparedStatement prepare2 = this .connect
														  .prepareStatement(
                                                            	"INSERT INTO j_emp_dev (jsd_id, jsd_emp_k, jsd_dev_k)"+
                                                            	"VALUES(?, ?, ?)"
                                                            );
						prepare2.setLong(1, id2);
						prepare2.setLong(2, obj.getId());
						prepare2.setLong(3, dev.getId());
						prepare2.executeUpdate();
					}
					
				}
				else{
					desarrolladorDAO.update(dev);
				}
				
			}
			
			obj = this.find(obj.getId());
			
		}catch(SQLException e){
			e.printStackTrace();
		}
		
		return obj;
		
	}

	public void delete(Empresa obj) {
		
		try	{
			this.connect.createStatement(
                ResultSet.TYPE_SCROLL_INSENSITIVE, 
                ResultSet.CONCUR_UPDATABLE
             ).executeUpdate(
                "DELETE FROM j_emp_dev WHERE jsd_emp_k = " + obj.getId()
             );
			this.connect.createStatement(
	                ResultSet.TYPE_SCROLL_INSENSITIVE, 
	                ResultSet.CONCUR_UPDATABLE
	            ).executeUpdate(
	                "DELETE FROM empresa WHERE emp_id = " + obj.getId()
	            );

	    } catch (SQLException e) {
	    	e.printStackTrace();
	    }
		
	}
	
}