package com.ejemplo.dao;

import java.sql.Connection;
import com.ejemplo.jdbc.ConnectionPostgreSQL;

public abstract class DAO<T> {

    public Connection connect = ConnectionPostgreSQL.getInstance();
	/**
	 * Permite recuperar un objeto por medio de su id
	 * @param id
	 * @return
	 */
	public abstract T find(long id);
	
	/**
	 * Permite crear una entrada en la base de datos
	 * para retornar un objeto
	 * @param obj
	 */
	public abstract T create(T obj);
	
	/**
	 * Permite actualizar el registro de un objeto en la Base de Datos
	 * @param obj
	 */
	public abstract T update(T obj);
	
	/**
	 * Permite eliminar una entrada de la Base de Datos
	 * @param obj
	 */
	public abstract void delete(T obj);
}

}