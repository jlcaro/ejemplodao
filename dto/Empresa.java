package com.ejemplo.dto;

import java.util.ArrayList;

public class Empresa {

	private long id = 0;
	private String nombre = "";
	private ArrayList<Desarrollador> listDesarrollador = new ArrayList<Desarrollador>();
	
	public Empresa(){}
	
	public Empresa(long id, String nombre, ArrayList<Desarrollador> listDesarrollador) {
		this.id = id;
		this.nombre = nombre;
		this.listDesarrollador = listDesarrollador;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}


	public String getNombre() {
		return nombre;
	}

	public void setNom(String nombre) {
		this.nombre = nombre;
	}

	public ArrayList<Desarrollador> getListDesarrollador() {
		return listDesarrollador;
	}

	public void setListDesarrollador(ArrayList<Desarrollador> listDesarrollador) {
		this.listDesarrollador = listDesarrollador;
	}
	
	public void addDesarrollador(Desarrollador dev){
		this.listDesarrollador.add(dev);
	}
	
	public Desarrollador getDesarrollador(int indice){
		return this.listDesarrollador.get(indice);
	}
	
	public String toString(){
		String str =	"*******************************\n";
		str += 			"NOMBRE : " + this.getNombre() + "\n";
		str +=			"*******************************\n";
		str +=			"LISTA DE DESARROLLADORES : \n";
		
		for(Desarrollador dev : this.listDesarrollador)
			str += dev.toString() + "\n";
		
		return str;
	}
}