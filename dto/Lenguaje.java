
package com.ejemplo.dto;

public class Lenguaje {
	private long id = 0;
	private String nombre = "";
	
	public Lenguaje(){}
	
	public Lenguaje(long id, String nombre){
		this.id = id;
		this.nombre = nombre;
	}

    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNom() {
		return nombre;
	}

	public void setNom(String nombre) {
		this.nombre = nombre;
	}
	
	public String toString(){
		return "LENGUAJE DE PROGRAMACION : " + this.nombre;
	}
	
}