package com.ejemplo.dto;

public class Desarrollador {
	private long id = 0;
	private String nombre = "", apellido = "";
	private Lenguaje lenguaje = new Lenguaje();
	
	public Desarrollador(){}
	
	public Desarrollador(long id, String nombre, String apellido, Lenguaje lenguaje) {
		this.id = id;
		this.nombre = nombre;
		this.apellido = apellido;
		this.lenguaje = lenguaje;
	}

    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}


	public String getNom() {
		return nombre;
	}

	public void setNom(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public Langage getLenguaje() {
		return lenguaje;
	}

	public void setLenguaje(Lenguaje lenguaje) {
		this.lenguaje = lenguaje;
	}
	
	public String toString(){
		String str = 	"NOM : " + this.getNom() + "\n";
		str += 			"APELLIDO : " + this.getApellido() + "\n";
		str +=			this.lenguaje.toString();
		str +=			"\n.....................................\n";
		
		return str;
	}
}